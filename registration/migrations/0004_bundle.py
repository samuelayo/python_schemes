# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-11 09:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0003_profile_phone_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bundle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=70)),
                ('amount', models.IntegerField(default=0)),
            ],
        ),
    ]
