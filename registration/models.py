from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver

class Bundle(models.Model):
    name = models.CharField(max_length=70, blank=False, null=False)
    amount = models.IntegerField(default=0)
    def __str__(self):
        return "%s (%s)" % (self.name, self.amount)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bank_name = models.CharField(max_length=70, blank=False, null=True)
    account_name = models.CharField(max_length=70, blank=False, null=True)
    account_number = models.CharField(max_length=70,unique=True, blank=False, null=True)
    phone_number = models.CharField(max_length=70,blank=False, null=True)
    current_bundle = models.ForeignKey(
        'Bundle',
        on_delete=models.CASCADE, null=True
    )
    def __str__(self):
        return "%s" % (self.user)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


