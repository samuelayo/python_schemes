from django import forms
from django.contrib.auth.models import User
from .models import *
class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username','first_name', 'last_name', 'email', 'password')

class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ['user',]

class UserLoginForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username','password',)