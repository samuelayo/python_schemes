from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import *
from donate.models import *
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login as reallogin, logout as reallogout
from django.contrib import messages 
from django.db.models import Q
import random
from django.utils import timezone
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
# Create your views here.

def home(request):
    return render(request,"home.html");
def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/home/')
    user_form = UserLoginForm()
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                reallogin(request, user)
                return redirect('/home/')
        else:
            messages.error(request, 'Invalid login credentials')
    return render(request,"login.html",  {
        'user_form': user_form
    });

def register(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/home/')
    user_form = UserForm()
    profile_form = ProfileForm()
    if request.method == 'POST':
        
        user_form = UserForm(request.POST)
        profile_form = ProfileForm(request.POST)
        if user_form.is_valid():
            
            if profile_form.is_valid():
                user_form=user_form.save()
                user_form.set_password(request.POST['password'])
                user_form.profile.bank_name = request.POST['bank_name']
                user_form.profile.account_name = request.POST['account_name']
                user_form.profile.account_number = request.POST['account_number']
                user_form.profile.phone_number = request.POST['phone_number']
                user_form.profile.current_bundle = Bundle.objects.get(pk=request.POST['current_bundle'])
                user_form.save()


                log = logs()
                log.user = User.objects.get(pk=user_form.id)
                log.action = "Your account was registered"
                log.date = timezone.now()
                log.save()


                ready = ReadyToDonate()
                ready.user = User.objects.get(pk=user_form.id)
                ready.bundle = Bundle.objects.get(pk=request.POST['current_bundle'])
                
                ready.save()

                log = logs()
                log.user = User.objects.get(pk=user_form.id)
                log.action = "You pledged to donate %s" % (ready.bundle.amount)
                log.date = timezone.now()
                log.save()


                existing = ReadyToDonate.objects.filter(Q(incoming1__isnull=True) | Q(incoming2__isnull=True) ).filter(completed_donate__isnull=False, bundle=Bundle.objects.get(pk=request.POST['current_bundle'])).order_by('created_at')
                print(len(existing))
                if len(existing) is not 0:
                    
                    this_one = existing[0]
                    donate_id = random.randint(1,100000000)
                    while ReadyToDonate.objects.filter(donate_id=donate_id):
                        donate_id = random.randint(1,100000000)
                    if this_one.incoming1 is None:
                        this_one.incoming1 = donate_id;
                    else: 
                        this_one.incoming2 = donate_id;
                    ready.donate_id = donate_id
                    ready.updated_at = timezone.now()
                    this_one.updated_at = timezone.now()
                    this_one.save()
                    ready.save()


                    log = logs()
                    log.user = User.objects.get(pk=user_form.id)
                    log.action = "You were merged to donate"
                    log.date = timezone.now()
                    log.save()

                    

            messages.success(request, 'Your profile was successfully created!')
            return redirect('/login')
        else:
            messages.error(request, 'Please correct the error below.')
    return render(request,"register.html",  {
        'user_form': user_form,
        'profile_form': profile_form
    });



def logout(request):
    reallogout(request)
    return redirect('/login')
    
def profile(request):
    from django.core.paginator import Paginator
    user_list = logs.objects.filter(user=request.user.id)
    page = request.GET.get('page', 1)

    paginator = Paginator(user_list, 10)
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
    return render(request, "profile.html", {
        'users': users,
        'user':request.user
    })

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('accounts:change_password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'accounts/change_password.html', {
        'form': form
    })



