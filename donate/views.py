from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.decorators import login_required
from .models import logs, ReadyToDonate
from registration.models import Bundle, Profile
from django.db.models import Q
from django.contrib.auth.models import User
from django.utils import timezone
import random
# Create your views here.
@login_required(login_url='/login/')
def donate(request):
    all_bundle = Bundle.objects.all()
    log = logs.objects.filter(user=request.user.id).order_by('-id')[:10]

    donate = ReadyToDonate.objects.filter(user=request.user.id).filter(donate_id__isnull= False).filter(completed_donate__isnull=True).order_by('-created_at')
    if len(donate) is 0:
        donate = None
        ref_donate = None
    else:
        donate_id = donate[0].donate_id;
        ref_donate = donate[0]
        donate = ReadyToDonate.objects.get(Q(incoming1=donate_id) | Q(incoming2=donate_id))

    incoming = ReadyToDonate.objects.filter(user=request.user.id).filter(donate_id__isnull= False).filter(completed_donate__isnull=False).order_by('-created_at')
    if len(incoming) is 0:
        incoming1 = None
        incoming1_ref = None
        incoming2 = None
        incoming2_ref = None
    else:
        if incoming[0].incoming1 is not None:
            incoming1 = ReadyToDonate.objects.filter(donate_id=incoming[0].incoming1)[0]
        else:
            incoming1 = None

        if incoming[0].incoming2 is not None:
            incoming2 = ReadyToDonate.objects.filter(donate_id=incoming[0].incoming2)[0]
        else:
           incoming2 = None
    last_attempt = ReadyToDonate.objects.filter(user=request.user.id).order_by("-id")
    if len(last_attempt) is 0:
        last_attempt = [None]
    return render(request,"donate.html", {
        'logs': log,
        'donate': donate,
        'donate_id': ref_donate,
        'incoming1':incoming1,
        'incoming2':incoming2,
        'last_attempt':last_attempt[0],
        'user':request.user,
        'bundles': all_bundle,
        'donations':ReadyToDonate.objects.all().count()
    });

@login_required(login_url='/login/')
def payment_made(request):
    if request.POST:
        donate_id = request.POST['paid']
        donate = ReadyToDonate.objects.get(donate_id=donate_id)
        donate.paid=1
        donate.save()
        log = logs()
        log.user = User.objects.get(pk=request.user.id)
        log.action = "You reported to have made donation"
        log.date = timezone.now()
        log.save()
        return redirect('/donate')
@login_required(login_url='/login/')
def confirm(request):
    if request.POST:
        donate_id = request.POST['paid']
        donate = ReadyToDonate.objects.get(donate_id=donate_id)
        donate.confirmed = 1
        donate.completed_donate = donate_id
        donate.save()
        log = logs()
        log.user = User.objects.get(pk=request.user.id)
        log.action = "You confirmed a donation"
        log.date = timezone.now()
        log.save()
        log = logs()
        log.user = User.objects.get(pk=donate.user.id)
        log.action = "You donation was confirmed"
        log.date = timezone.now()
        log.save()
        #check if both completions are done
        sponsor =  ReadyToDonate.objects.get(Q(incoming1= donate_id) | Q(incoming2=donate_id) )
        if sponsor is not None:
            completed = 0;
            if sponsor.incoming1 is not None:
                incoming1 = ReadyToDonate.objects.get(donate_id=sponsor.incoming1)
                if incoming1.completed_donate is not None:
                    completed +=1
            if sponsor.incoming2 is not None:
                incoming2 = ReadyToDonate.objects.get(donate_id=sponsor.incoming2)
                if incoming2.completed_donate is not None:
                    completed +=1
            if completed==2:
                ret = Profile.objects.get(user=request.user)
                ret.current_bundle = None
                ret = ret.save()
                request.user.save()
                

        return redirect('/donate')


@login_required(login_url='/login/')
def purge_user(request):
     if request.POST:
        donate_id = request.POST['paid']
        merged_user = ReadyToDonate.objects.get(Q(incoming1=donate_id) | Q(incoming2=donate_id))
        if str(merged_user.incoming1) == str(donate_id):
            merged_user.incoming1 = None
        if str(merged_user.incoming2) == str(donate_id):
            merged_user.incoming2 = None
        merged_user.save()
        donate = ReadyToDonate.objects.get(donate_id=donate_id)
        user = User.objects.get(pk=donate.user.id);
        user.is_active = 0;
        user.save()
        donate.delete()

        log = logs()
        log.user = User.objects.get(pk=request.user.id)
        log.action = "You purged a non responsive user"
        log.date = timezone.now()
        log.save()
        return redirect('/donate')


def unreachable(request):
    if request.POST:
        donate_id = request.POST['paid']
        merged_user = ReadyToDonate.objects.get(Q(incoming1=donate_id) | Q(incoming2=donate_id))
        if str(merged_user.incoming1) == str(donate_id):
            merged_user.incoming1 = None
        if str(merged_user.incoming2) == str(donate_id):
            merged_user.incoming2 = None
        merged_user.updated_at = timezone.now() + timezone.timedelta(hours=2)
        merged_user.created_at = timezone.now() + timezone.timedelta(hours=2)
        merged_user.save()
        donate = ReadyToDonate.objects.get(donate_id=donate_id)
        donate.donate_id=None
        donate.save()

        log = logs()
        log.user = User.objects.get(pk=request.user.id)
        log.action = "You declared that your sponsor was unreachable"
        log.date = timezone.now()
        log.save()
        return redirect('/donate')
    return 0



def recycle(request):
    if request.POST:
        bundle = request.POST['bundle']
        nw = Bundle.objects.get(id=int(bundle))
        log = logs()
        log.user = User.objects.get(pk=request.user.id)
        log.action = "You recycled to donate %s on %s package" % (nw.amount, nw.name)
        log.date = timezone.now()
        log.save()
        donate = ReadyToDonate()
        donate.user=request.user
        donate.bundle = Bundle.objects.get(id=bundle)
        donate.save()

        ret = Profile.objects.get(user=request.user)
        ret.current_bundle = nw
        ret = ret.save()
        request.user.save()
        
        return redirect('/donate')

def recheckmerge(request):
    #remove the onces that has exceeded 1 hour
    paired_to_remove = ReadyToDonate.objects.exclude(donate_id=None).filter(completed_donate=None)
    for is_elapsed in paired_to_remove:
        if timezone.now() >= (is_elapsed.updated_at + timezone.timedelta(hours=2)):
            donate_id = is_elapsed.donate_id
            merged_user = ReadyToDonate.objects.get(Q(incoming1=is_elapsed.donate_id) | Q(incoming2=is_elapsed.donate_id))
            if str(merged_user.incoming1) == str(donate_id):
                merged_user.incoming1 = None
               
            if str(merged_user.incoming2) == str(donate_id):
                merged_user.incoming2 = None
            log = logs()
            log.user = User.objects.get(pk=merged_user.user.id)
            log.action = "Your downliner was removed for not paying automatically"
            log.date = timezone.now()
            log.save()
            merged_user.save()
            #block User
            the_user = User.objects.get(id=is_elapsed.user.id)
            the_user.is_active = 0;
            the_user.save()
            #remove his donation
            is_elapsed.delete()
            
    existing = ReadyToDonate.objects.filter(~Q(completed_donate =None)).filter(~Q(donate_id =None)).exclude(incoming1__isnull=False , incoming2__isnull=False).order_by('created_at')
    for this_one in existing:
    
        towho=ReadyToDonate.objects.filter(donate_id=None, bundle=Bundle.objects.get(pk=this_one.bundle.id))
        if len(towho) is not 0:
            ready = towho[0]
            donate_id = random.randint(1,100000000)
            while ReadyToDonate.objects.filter(donate_id=donate_id):
                donate_id = random.randint(1,100000000)
            if this_one.incoming1 is None:
                this_one.incoming1 = donate_id;
            else: 
                this_one.incoming2 = donate_id;
            ready.donate_id = donate_id
            ready.updated_at = timezone.now()
            log = logs()
            log.user = User.objects.get(pk=ready.user.id)
            log.action = "Your were merged to make payment"
            log.date = timezone.now()
            log.save()
            this_one.updated_at = timezone.now()
            this_one.save()
            log = logs()
            log.user = User.objects.get(pk=this_one.user.id)
            log.action = "Your were merged to receive payment"
            log.date = timezone.now()
            log.save()
            ready.save()
    return HttpResponse("done")





