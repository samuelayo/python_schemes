from django.contrib import admin
from .models import ReadyToDonate, logs
from registration.models import Bundle
# Register your models here.
class logsAdmin(admin.ModelAdmin):
    list_display = ('user', 'action', 'date',)
    search_fields = ('user__username',)
class ReadyToDonateAdmin(admin.ModelAdmin):
    list_display = ('user', 'donate_id', 'paid','completed_donate', 'created_at', 'updated_at', 'incoming1', 'incoming2')
    search_fields = ('user__username',)
admin.site.register(ReadyToDonate, ReadyToDonateAdmin)
admin.site.register(Bundle)
admin.site.register(logs, logsAdmin)
