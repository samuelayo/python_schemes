from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.
class ReadyToDonate(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE, null=True
    )
    bundle = models.ForeignKey(
        'registration.Bundle',
        on_delete=models.CASCADE, null=True
    )
    donate_id = models.BigIntegerField(null=True)
    paid = models.IntegerField(null=True, default=0)
    confirmed = models.IntegerField(null=True, default=0)
    elapsed = models.IntegerField(null=True, default=0)
    completed_donate = models.BigIntegerField(null=True)
    incoming1 = models.BigIntegerField(null=True, blank=True, default=None)
    incoming2 = models.BigIntegerField(null=True, blank=True, default=None)
    created_at = models.DateTimeField(default=timezone.now())
    updated_at = models.DateTimeField(default=timezone.now())
    def __str__(self):
        return "Donation for %s" % (self.user)

class logs(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE, null=True
    )
    action = models.TextField(max_length=None)
    date = models.DateField(auto_now=True)