"""scheme URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from registration import views
from django.contrib.auth import views as auth_views
from donate import views as donateview

urlpatterns = [
    url(r'^$', views.home),
    url(r'^admin/', admin.site.urls),
    url(r'^login', views.login),
    url(r'^logout$', views.logout),
    url(r'^register$', views.register),
    url(r'^donate$', donateview.donate),
    url(r'^home', donateview.donate),
    url(r'^payment_made', donateview.payment_made),
    url(r'^confirm', donateview.confirm),
    url(r'^purge_user', donateview.purge_user),
    url(r'^unreachable', donateview.unreachable),
    url(r'^profile', views.profile),
    url(r'^password/$', views.change_password, name='change_password'),
    url(r'^recycle$', donateview.recycle),
    url(r'^respin$', donateview.recheckmerge),
    url('^', include('django.contrib.auth.urls')),

]
admin.site.site_header = 'Primegivers'
admin.site.site_title = 'Primegivers'